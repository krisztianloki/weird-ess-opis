from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg    = None

try:
    code_str = PVUtil.getString(pvs[0])

    msgs = dict({
                "000000" : "",
                # Errors
                "Err001" : [ "Excess rotation speed",
                             "",
                             "+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                           ],
                "Err002" : [ "Overvoltage",
                             "- Incorrect mains input voltage",
                             "+ Check mains input voltage\n+ Only acknowledge for rotation speed f = 0\n+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err006" : [ "Run-up time error",
                             "- Run-up time threshold set too low\n- Gas flow in vacuum chamber through leaks or open valves\n- Still below rotation speed switchpoint run-up time expires",
                             "+ Adjust run-up time to process conditions\n+ Check vacuum chamber for leaks and closed valves\n+ Check backup vacuum connection\n+ Adjust rotation speed switchpoint"
                           ],
                "Err007" : [ "Operating fluid low",
                             "- Operating fluid low",
                             "+ Check operating fluid\n+ Only acknowledge for rotation speed f = 0\n+ Can be acknowledged a max. of 5 times\n+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err015" : [ "Group error message control unit",
                             "",
                             "+ Mains OFF/ON at rotation speed f = 0\n+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err021" : [ "Electronic drive unit failed to identify pump",
                             "- Incorrect characteristic resistance\n- Pump not connected",
                             "+ Check connections\n+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                           ],
                "Err037" : [ "Motor final stage or control error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err040" : [ "Memory expansion error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err043" : [ "Internal configuration error",
                             "- Parameter values stored incorrectly",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err044" : [ "Excess temperature electronics",
                             "- Insufficient cooling",
                             "+ Improve cooling\n+ Check deployment conditions"
                           ],
                "Err045" : [ "Motor temperature protection",
                             "- Motor overheated\n- Run-up time in lower speed range (up to 90 Hz) > 6 min",
                             "+ Improve cooling\n+ Check fore-vacuum connection\n   - Perform leak detection\n   - Reduce fore-vacuum pressure"
                           ],
                "Err098" : [ "Internal communication error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err621" : [ "Electronic drive unit failed to identify pump",
                             "- Incorrect characteristic resistance\n- Pump not connected",
                             "+ Check connections\n+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                           ],
                "Err699" : [ "Error in TCP drive",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Err777" : [ "Nominal speed not confirmed",
                             "- Nominal speed not confirmed after replacing electronic drive unit",
                             "+ Confirm nominal speed with [P:777]\n+ Only acknowledge for rotation speed f = 0"
                           ],
                # Warnings
                "Wrn007" : [ "Undervoltage/mains failure",
                             "- Mains failure",
                             "+ Check power supply"
                           ],
                "Wrn046" : [ "Data channel error",
                             "- Communication to parameter value memory faulty",
                             "+ Contact Pfeiffer Vacuum Service"
                           ],
                "Wrn110" : [ "Gauge warning",
                             "- Gauge faulty\n- Supply cable worked loose during operation",
                             "+ Restart with gauge connected\n+ Replace gauge\n+ Install gauge correctly"
                           ],
                })


    try:
        msg = msgs[code_str]
    except KeyError:
        msg = "\n{}: Unknown error code".format(code_str)
except Exception as err:
    ScriptUtil.getLogger().severe(str(err))

tooltip = "$(pv_name)\n$(pv_value)"

if msg is not None:
    if isinstance(msg, list):
        tooltip = tooltip + "\n{code}: {msg}\nPossible causes:\n{causes}\nRemedy:\n{remedies}".format(code = code_str, msg = msg[0], causes = msg[1], remedies = msg[2])
    else:
        tooltip = tooltip + "\n{}".format(msg)

widget.setPropertyValue("tooltip", tooltip)
