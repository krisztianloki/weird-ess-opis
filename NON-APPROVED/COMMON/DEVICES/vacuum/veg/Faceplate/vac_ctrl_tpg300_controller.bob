<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICENAME)</name>
  <width>610</width>
  <height>376</height>
  <widget type="group" version="2.0.0">
    <name>MKS - 937B / 946</name>
    <width>610</width>
    <height>376</height>
    <style>2</style>
    <foreground_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <widget type="label" version="2.0.0">
      <name>Configuration</name>
      <text>$(DEVICENAME)</text>
      <width>610</width>
      <height>35</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
      <transparent>false</transparent>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>Connected</name>
      <pv_name>$(DEVICENAME):CommsOK</pv_name>
      <x>359</x>
      <y>10</y>
      <width>15</width>
      <height>15</height>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="text-symbol" version="2.0.0">
      <name>Text Symbol</name>
      <pv_name>$(DEVICENAME):CommsOK</pv_name>
      <symbols>
        <symbol>No Communication with Device</symbol>
        <symbol>Communication is  OK</symbol>
      </symbols>
      <x>380</x>
      <y>10</y>
      <width>210</width>
      <height>15</height>
      <font>
        <font name="SMALL-SERIF-BOLD" family="Source Serif Pro" style="BOLD" size="14.0">
        </font>
      </font>
      <foreground_color>
        <color name="WHITE" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>0</horizontal_alignment>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Module Configuration</name>
      <file>vac_ctrl_tpg300_module.bob</file>
      <x>10</x>
      <y>50</y>
      <width>590</width>
      <height>151</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="group" version="2.0.0">
      <name>System Settings</name>
      <x>10</x>
      <y>221</y>
      <width>590</width>
      <height>145</height>
      <style>2</style>
      <foreground_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
      <widget type="label" version="2.0.0">
        <name>System Settings</name>
        <text>System Settings</text>
        <width>590</width>
        <height>30</height>
        <font>
          <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
          </font>
        </font>
        <foreground_color>
          <color name="GRAY-TEXT" red="255" green="255" blue="255">
          </color>
        </foreground_color>
        <background_color>
          <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
          </color>
        </background_color>
        <transparent>false</transparent>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Pressure Unit</name>
        <text>Pressure Unit:</text>
        <y>40</y>
        <width>150</width>
        <height>25</height>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>Pressure UnitS</name>
        <pv_name>${DEVICENAME}:UnitS</pv_name>
        <x>160</x>
        <y>40</y>
        <width>220</width>
        <height>25</height>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Pressure UnitR</name>
        <pv_name>$(DEVICENAME):Unit-RB</pv_name>
        <x>386</x>
        <y>40</y>
        <width>194</width>
        <height>25</height>
        <format>6</format>
        <show_units>false</show_units>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Underrange Control</name>
        <text>Underrange Control:</text>
        <y>75</y>
        <width>150</width>
        <height>25</height>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>UnderRngCtrlS</name>
        <pv_name>${DEVICENAME}:UnderRngCtrlS</pv_name>
        <x>160</x>
        <y>75</y>
        <width>220</width>
        <height>25</height>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>UnderRngCtrl-RB</name>
        <pv_name>$(DEVICENAME):UnderRngCtrl-RB</pv_name>
        <x>386</x>
        <y>75</y>
        <width>194</width>
        <height>25</height>
        <format>6</format>
        <show_units>false</show_units>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Parameter Storage</name>
        <text>Parameter Storage:</text>
        <y>110</y>
        <width>150</width>
        <height>25</height>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>SaveParamsS</name>
        <pv_name>$(DEVICENAME):SaveParamsS</pv_name>
        <x>160</x>
        <y>110</y>
        <width>220</width>
        <height>25</height>
        <actions>
        </actions>
        <tooltip>$(pv_name)$(pv_value)</tooltip>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>SaveParams-RB</name>
        <pv_name>$(DEVICENAME):SaveParams-RB</pv_name>
        <x>386</x>
        <y>110</y>
        <width>194</width>
        <height>25</height>
        <format>6</format>
        <show_units>false</show_units>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
    </widget>
  </widget>
</display>
