PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvHealthy   = 0;
var pvTripped   = 0;
var pvOverriden = 0;
var pvDisabled  = 0;

var pvSymbol    = pvs[0];

var sum     = 0;
var isValid = 0;
var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvHealthy   = 1 * PVUtil.getInt(pvs[1]);
	pvTripped   = 2 * PVUtil.getInt(pvs[2]);
	pvOverriden = 4 * PVUtil.getInt(pvs[3]);
	pvDisabled  = 1 * PVUtil.getInt(pvs[4]);

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	sum         = pvHealthy | pvTripped | pvOverriden | pvDisabled;
	isValid     = (sum & (sum - 1)) == 0 ? 1 : 0;

	if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvTripped) {
		Logger.info(pvSymbol + ": TRIPPED");
		colorID = 2;
	} else if (pvOverriden) {
		Logger.info(pvSymbol + ": OVERRIDEN");
		colorID = 3;
	} else if (pvDisabled) {
		Logger.info(pvSymbol + ": DISABLED");
		colorID = 4;
	} else if (pvHealthy) {
		Logger.info(pvSymbol + ": HEALTHY");
		colorID = 1;
	} else {
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);
	}

	if (colorID != PVUtil.getInt(pvSymbol))
		Logger.severe(pvSymbol + ": State mismatch:" + colorID + " vs " + PVUtil.getInt(pvSymbol));
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}

//pvSymbol.write(colorID);
