<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>TS2 PSS Electrical Cabinet</name>
  <width>260</width>
  <height>300</height>
  <widget type="group" version="2.0.0">
    <name>Cabinet</name>
    <width>260</width>
    <height>300</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>BGGrey03-background</name>
      <width>260</width>
      <height>300</height>
      <line_width>0</line_width>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>5</corner_width>
      <corner_height>5</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>$(CABINET)</text>
      <width>260</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Signals</name>
      <x>10</x>
      <y>40</y>
      <width>240</width>
      <height>250</height>
      <style>3</style>
      <widget type="led" version="2.0.0">
        <name>LED_UPSAlarm</name>
        <pv_name>$(P):UPSAlarm</pv_name>
        <x>25</x>
        <y>10</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-RED-OFF" red="110" green="101" blue="90">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSAlarm</name>
        <text>UPS Alarm</text>
        <x>51</x>
        <y>10</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_SurgeArrOK</name>
        <pv_name>$(P):SurgeArrOK</pv_name>
        <x>25</x>
        <y>40</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_SurgeArrOK</name>
        <text>Surge Arrester OK</text>
        <x>51</x>
        <y>40</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_PS24vOK</name>
        <pv_name>$(P):PS24vOK</pv_name>
        <x>25</x>
        <y>70</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_PS24vOK</name>
        <text>Power Supply OK</text>
        <x>51</x>
        <y>70</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBatOK</name>
        <pv_name>$(P):UPSBatOK</pv_name>
        <x>25</x>
        <y>100</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBatOK</name>
        <text>UPS 24V from Battery</text>
        <x>51</x>
        <y>100</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPS24vOK</name>
        <text>UPS 24V from PSU</text>
        <x>51</x>
        <y>130</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPS24vOK</name>
        <pv_name>$(P):UPS24vOK</pv_name>
        <x>25</x>
        <y>130</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBuffReady</name>
        <pv_name>$(P):UPSBuffReady</pv_name>
        <x>25</x>
        <y>160</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBuffReady</name>
        <text>UPS Ready to Buffer</text>
        <x>51</x>
        <y>160</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBatt85</name>
        <pv_name>$(P):UPSBatt85</pv_name>
        <x>25</x>
        <y>190</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBatt85</name>
        <text>UPS Battery &gt; 85%</text>
        <x>51</x>
        <y>190</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_SMAlarm</name>
        <pv_name>$(P):SMAlarm</pv_name>
        <x>25</x>
        <y>220</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_SMAlarm</name>
        <text>Selectivity Module</text>
        <x>51</x>
        <y>220</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
    </widget>
  </widget>
</display>
